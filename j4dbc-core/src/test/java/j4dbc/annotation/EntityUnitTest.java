package j4dbc.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.junit.Assert;
import org.junit.Test;

public class EntityUnitTest {

    @Test
    public void testEntityAnnotation_shouldHasAnnotationRetention_shouldBeRetentionValueEqualsToSource() {
	
	Class<Entity> entity = Entity.class;
	
	Retention retention = entity.getAnnotation(Retention.class);
	
	Assert.assertNotNull(retention);
	Assert.assertEquals(retention.value(), RetentionPolicy.SOURCE);
    }
    
    @Test
    public void testEntityAnnotation_shouldHasAnnotationTarget_shouldHasTargetValueOnlyOneElementType_shouldBeTargetValueEqualsType() {
	
	Class<Entity> entity = Entity.class;
	
	Target target = entity.getAnnotation(Target.class);
	
	Assert.assertNotNull(target);
	Assert.assertEquals(target.value().length, 1);
	Assert.assertEquals(target.value()[0], ElementType.TYPE);
    }
}
